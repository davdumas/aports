# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=krfb
pkgver=22.04.1
pkgrel=0
# armhf, s390x and riscv64 blocked by kwallet-dev and kxmlgui-dev
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/internet/org.kde.krfb"
pkgdesc="Desktop sharing"
license="GPL-3.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kwallet-dev
	kwayland-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libvncserver-dev
	pipewire-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	xcb-util-dev
	xcb-util-image-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/krfb-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4553e7e826a240d1634769a55ddf70b267da382f096798024f13b96bc63c8d487a9e0aa58ee69b16d17b24ad81b08e1f334ba2ba2eef122844b4aa73d6d70da7  krfb-22.04.1.tar.xz
"
