# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-calendar-tools
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by akonadi-calendar -> kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="CLI tools to manage akonadi calendars"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-calendar-dev
	akonadi-dev>=$pkgver
	calendarsupport-dev
	extra-cmake-modules
	kcalendarcore-dev
	kcalutils-dev
	kdoctools-dev
	libkdepim-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-calendar-tools-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
273c5033e41aa252a8edc1e069ea24d99bb0c4f78e48d8e66b638489f39d9db7a25ac347777c18ac32c98efb892f2a9c67f3c3b672f9fc337fe6011b3f39025d  akonadi-calendar-tools-22.04.1.tar.xz
"
