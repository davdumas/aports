# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=borgmatic
pkgver=1.6.2
pkgrel=0
pkgdesc="Simple, configuration-driven backup software for servers and workstations"
url="https://torsion.org/borgmatic/"
license="GPL-3.0-or-later"
arch="noarch !s390x" # tests fail on s390x
depends="
	borgbackup
	python3
	py3-setuptools
	py3-jsonschema
	py3-requests
	py3-ruamel.yaml
	py3-colorama
	"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-flexmock
	"
source="$pkgname-$pkgver.tar.gz::https://projects.torsion.org/borgmatic-collective/borgmatic/archive/$pkgver.tar.gz"
builddir="$srcdir/borgmatic"

build() {
	python3 setup.py build
}

check() {
	# omit a simple test that requires borgmatic to be available in $PATH
	pytest -k "not test_borgmatic_version_matches_news_version"
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir" --skip-build
}

sha512sums="
e443278a77c4026714eef03cf49fbf4fc65bf33a2501d402667917b97ab25b9bc11a9517bbb64b4a49bbdd75841d458bf61ba5eec9720cbac1645e78b3ebce00  borgmatic-1.6.2.tar.gz
"
