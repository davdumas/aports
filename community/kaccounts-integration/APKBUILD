# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kaccounts-integration
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, ppc64le and riscv64 blocked by signon-ui and kdeclarative
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kde.org/applications/internet/"
pkgdesc="Small system to administer web accounts for the sites and services across the KDE desktop"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	accounts-qml-module
	signon-ui
	"
depends_dev="
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	libaccounts-qt-dev
	qt5-qtbase-dev
	samurai
	signond-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaccounts-integration-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # No tests available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8b5b77708930424a9faf452fdc11da0c7c1a2c608c57ba732007691e85a703c4af1b0ef0dcbff3f63d38e8909414317a2617bafb2a12af97a3e6d1adeb17c6a9  kaccounts-integration-22.04.1.tar.xz
"
