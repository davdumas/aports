# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalzium
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> khtml
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/kalzium/"
pkgdesc="Periodic Table of Elements"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	eigen-dev
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	knewstuff-dev
	kparts-dev
	kplotting-dev
	kunitconversion-dev
	kwidgetsaddons-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalzium-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5b86ac08086a429ac3b07533320d983b49f8a8c8acc23f3d7c21e9881f0686e114c7b040a66a25ccf30e217bf15d7db722a32350bcb5ce1b4c646d080f420369  kalzium-22.04.1.tar.xz
"
