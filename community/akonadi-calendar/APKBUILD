# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-calendar
pkgver=22.04.1
pkgrel=0
pkgdesc="Akonadi calendar integration"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev>=$pkgver
	akonadi-dev>=$pkgver
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kio-dev
	kmailtransport-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	samurai
	"
makedepends="$depends_dev
	extra-cmake-modules
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-calendar-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Broken

replaces="kalendar>1.0.0"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
0256c666db678775ca6e6d44dbe5f1aa2ac5b37f40c2abfc44b76f28ba5bab3ebe32c05192f86d49428061ad6dc74079f66ed9d1b6fb4e925ef789daf57a4a18  akonadi-calendar-22.04.1.tar.xz
"
