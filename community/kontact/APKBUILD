# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kontact
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Container application to unify several major PIM applications within one application"
license="GPL-2.0-or-later"
makedepends="
	akonadi-dev
	extra-cmake-modules
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kiconthemes-dev
	kontactinterface-dev
	kpimtextedit-dev
	kwindowsystem-dev
	libkdepim-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kontact-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bb736397edb3482ffdd3614e8eeecc4f682dcc3763d1cff5ff3b61660a62cb2b4c918adfd8ba639e3e9954dc198853557b80e318213f9b90a906b69d680e6d7a  kontact-22.04.1.tar.xz
"
