# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=gwenview
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
# ppc64le blocked by qt5-qtwebengine -> purpose
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://kde.org/applications/graphics/org.kde.gwenview"
pkgdesc="Fast and easy to use image viewer by KDE"
license="GPL-2.0-only"
depends="kimageformats"
makedepends="
	baloo-dev
	extra-cmake-modules
	kactivities-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	knotifications-dev
	kparts-dev
	kwindowsystem-dev
	lcms2-dev
	libjpeg-turbo-dev
	libkdcraw-dev
	libkipi-dev
	libpng-dev
	purpose-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="xvfb-run kinit dbus"
source="https://download.kde.org/stable/release-service/$pkgver/src/gwenview-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# urlutilstest and placetreemodeltest are broken
	# recursivedirmodeltest and contextmanagertest requires running DBus
	local skipped_tests="("
	local tests="
		contextmanager
		documenttest
		jpegcontenttest
		placetreemodel
		recursivedirmodel
		urlutils
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	CTEST_OUTPUT_ON_FAILURE=TRUE dbus-run-session xvfb-run ctest -E "$skipped_tests"

}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
60b4c141e897a36db7c1a646da70322cd16744d8e0840c78b16d3f5caa797206014b551c9681d1a38c26a641bf65048304af70fd57fcca09be88cee5408285ed  gwenview-22.04.1.tar.xz
"
