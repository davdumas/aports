# Contributor: Leon Marz <main@lmarz.org>
# Maintainer: Leon Marz <main@lmarz.org>
pkgname=openxr
pkgver=1.0.23
pkgrel=0
pkgdesc="OpenXR loader library"
url="https://khronos.org/openxr"
arch="all"
license="Apache-2.0"
makedepends="cmake mesa-dev vulkan-loader-dev jsoncpp-dev samurai wayland-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/KhronosGroup/OpenXR-SDK/archive/release-$pkgver.tar.gz"
builddir="$srcdir/OpenXR-SDK-release-$pkgver"
options="!check" # no check available

build() {
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=Release
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cb73e1ca5e7845f2735956d375d41cd1ed0363f5ca7b8d88a742cd2a496ad638b01e0395072a1ad097069c1679c1e2f4bf43ed0784560f415b1762134f7a6860  release-1.0.23.tar.gz
"
